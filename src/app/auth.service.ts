import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  constructor() { }
  
  authorize(){
    localStorage.removeItem('token')

    let client_id = '176d33733fe84eff814ffb5c179b8dea'
    let redirect_uri = 'http://localhost:4200/'

    let url = `https://accounts.spotify.com/authorize?client_id=${client_id}&response_type=token&redirect_uri=${redirect_uri}`

    location.replace(url)
  }

  getToken(){
    let token = JSON.parse(localStorage.getItem('token'))

    if(!token){
        let match = location.hash.match(/#access_token=(.*?)&/)
        token = match && match[1]
    }
    if(!token){
      this.authorize()
    }else{
      localStorage.setItem('token', JSON.stringify(token))
    }
    return token;
  }

}
