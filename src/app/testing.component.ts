import { Component, Inject, OnInit } from '@angular/core';

@Component({
  selector: 'testing',
  template: `
    <p>{{message}}</p>
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  constructor(@Inject('placki') public service) { }

  message = 'placki'

  ngOnInit() {
  }

}
