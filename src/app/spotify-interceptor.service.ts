import { AuthService } from './auth.service';
import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/catch'

@Injectable()
export class SpotifyInterceptorService implements HttpInterceptor {

  intercept(req:HttpRequest<any>, next:HttpHandler) {
    
    var request = req.clone({
      setHeaders:{
      'Authorization': 'Bearer ' + this.auth.getToken()
      }
    })
    
    return next.handle(request)
    .catch((err,caught) => {
      // Assume 401 - token expired
      this.auth.authorize()
      return caught
    })
  }

  constructor(private auth:AuthService) { }

}
