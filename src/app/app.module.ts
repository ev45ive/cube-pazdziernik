
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { AuthService } from './auth.service';
import { MusicSearchModule } from './music-search/music-search.module';
import { FormsModule } from '@angular/forms';

import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, ComponentFactoryResolver, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistItemComponent } from './playlists/playlist-item.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { SpotifyInterceptorService } from './spotify-interceptor.service';

import { Routing } from './app.routing';
import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { PlaylistsService } from './playlists/playlists.service';
import { TestingComponent } from './testing.component';

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistItemComponent,
    PlaylistDetailsComponent,
    PlaylistContainerComponent,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    Routing,
    MusicSearchModule
  ],
  providers: [
    { 
      provide: HTTP_INTERCEPTORS, 
      useClass: SpotifyInterceptorService,
      multi: true 
    },
    PlaylistsService,
    AuthService
  ],
  entryComponents: [AppComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private auth:AuthService, 
    // private app: ApplicationRef, private factory: ComponentFactoryResolver
  ){
      this.auth.getToken()
  }
  // ngDoBootstrap(){
  //     // console.log('manual bootstrap')
  //     let factory = this.factory.resolveComponentFactory(AppComponent)
  //     this.app.bootstrap(factory)
  // }

}
