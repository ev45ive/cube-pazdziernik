import { PlaylistContainerComponent } from './playlists/playlist-container.component';
import { MusicSearchComponent } from './music-search/music-search.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { RouterModule, Routes } from '@angular/router';

const routes:Routes = [
    { path: '', redirectTo:'playlists', pathMatch: 'full' },
    { path: 'playlists', component: PlaylistsComponent,
        children: [
            {path:'', component: PlaylistContainerComponent },
            {path:':id', component: PlaylistContainerComponent }
        ] 
    },
    { path: 'music', component: MusicSearchComponent },
    { path: '**', redirectTo:'music', pathMatch: 'full'},
]


export const Routing = RouterModule.forRoot(routes,{
    enableTracing: false,
    useHash: true
})