import { AuthService } from '../auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Album } from './interfaces';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/filter';
import { Observable } from 'rxjs/Observable';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/switchMap';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MusicService {

  constructor(private http:HttpClient) {
 
    this.queries$
    .filter( query => query.length >= 3)
    .map( query =>  this.url(query) )
    .switchMap( url => this.http.get(url) )
    .map( data => <Album[]> data['albums'].items )
    .subscribe( this.albums$ )
  }

  private url(query){
    return `https://api.spotify.com/v1/search?type=album&market=PL&q=${query}`
  }

  queries$ = new BehaviorSubject<string>('Superman')
  
  search(query){
    this.queries$.next(query)
  }

  albums$ = new BehaviorSubject<Album[]>([])

  getAlbums(query = 'batman'){
    return this.albums$.asObservable()
  }

}
