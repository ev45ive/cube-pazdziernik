import { MusicService } from './music.service';
import { Component, Input, OnInit } from '@angular/core';
import { Album } from './interfaces';

@Component({
  selector: 'albums-list',
  template: `
    <div class="card-group">
      <div class="card" 
        *ngFor="let album of albums$ | async "
        [album-item]="album"></div>
    </div>
  `,
  styles: [`
    .card{
      min-width:25%;
      max-width:25%;
    }
  `]
})
export class AlbumsListComponent implements OnInit {

  constructor(private service:MusicService) { }

  albums$ 

  ngOnInit() {
    this.albums$ = this.service.getAlbums()
  }

}
