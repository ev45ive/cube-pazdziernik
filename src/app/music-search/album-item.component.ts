import { Album, AlbumImage } from './interfaces';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'album-item, [album-item]',
  template: `
    <img class="card-img-top" [src]="image.url" [width]="image.width">
    <div class="card-body">
      <h4 class="card-title">
        {{album.name}}
      </h4>
    </div>
  `,
  styles: []
})
export class AlbumItemComponent implements OnInit {

  @Input('album-item')
  set onAlbum(album){
    this.album = album
    this.image = album.images[0]
  }
  image:AlbumImage

  album: Album

  constructor() { }

  ngOnInit() {
  }

}
