import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SpotifyInterceptorService } from '../spotify-interceptor.service';
import { MusicService } from './music.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { SearchFormComponent } from './search-form.component';
import { AlbumsListComponent } from './albums-list.component';
import { AlbumItemComponent } from './album-item.component';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  declarations: [
    MusicSearchComponent, 
    SearchFormComponent, 
    AlbumsListComponent, 
    AlbumItemComponent
  ],
  providers:[
    MusicService
  ],
  exports:[
    MusicSearchComponent
  ]
})
export class MusicSearchModule { }
