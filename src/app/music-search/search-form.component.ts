import { Observable } from 'rxjs/Rx';
import { FormControlName, FormGroupName } from '@angular/forms/src/directives';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup,Validators, ValidatorFn, ValidationErrors, Validator, AsyncValidatorFn } from '@angular/forms';
import { Component, Input, OnInit, Output } from '@angular/core';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/debounceTime';

@Component({
  selector: 'search-form',
  template: `
  <form [formGroup]="queryForm">
    <div class="input-group mb-2">
        <input type="text" class="form-control" formControlName="query">
    </div>
    <div *ngIf="queryForm.pending">Please wait... Checking.</div>
    <ng-container *ngIf="queryForm.get('query').touched || queryForm.get('query').dirty">
      <div *ngIf="queryForm.get('query').hasError('required')">Field is required</div>
      <div *ngIf="queryForm.get('query').hasError('minlength')">Value too short!</div>
      <div *ngIf="queryForm.get('query').hasError('badword')">Value too bad!
          {{ queryForm.get('query').errors.badword }}
      </div>
    </ng-container>
  </form>
  `,
  styles: [`
    form .ng-invalid.ng-touched,
    form .ng-invalid.ng-dirty {
        border: 2px solid red;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  @Input()
  set query(query){
    //this.queryForm.get('query').setValue(query)
  }

  queryForm: FormGroup

  constructor(private builder: FormBuilder) {

    var censor = (badword:string):ValidatorFn => (control:AbstractControl):ValidationErrors => {
      
      let hasError = control.value.indexOf(badword) !== -1
      
      return hasError? {
        badword: badword
      } : null;
    }

    var asyncCensor = (badword:string):AsyncValidatorFn => (control:AbstractControl):Observable<ValidationErrors | null > => {

      return Observable.create( observer => {
        setTimeout(()=>{

            let hasError = control.value.indexOf(badword) !== -1
            
            observer.next(hasError? {
              badword: badword
            } : null)
            observer.complete()
        }, 2000)
      })

      // this.http.get().map( reponse => error )
    }


    this.queryForm = this.builder.group({
      'query': this.builder.control('',[
        Validators.required,
        Validators.minLength(3),
        // censor('batman')
      ],[
        asyncCensor('batman')
      ])
    })
    window['form'] = this.queryForm

    this.queryChange = this.queryForm.get('query').valueChanges
    .debounceTime(400)
    .filter( query => query.length >= 3)
    .distinctUntilChanged()
    // .subscribe( value => this.search(value) )
  }

  @Output()
  queryChange

  ngOnInit() {
  }

}
