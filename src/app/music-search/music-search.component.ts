import { MusicService } from './music.service';
import { Component, OnInit, Inject } from '@angular/core';
import { Album } from './interfaces';

@Component({
  selector: 'music-search',
  template: `
    <div class="row">
    <div class="col">
      <search-form 
      [query]="query$ | async"
      (queryChange)="search($event)"></search-form>
    </div>
    </div>
    <div class="row">
    <div class="col">
      <albums-list></albums-list>
    </div>
    </div>
`, styles: []
})
export class MusicSearchComponent implements OnInit {

  albums:Album[] = []
  
  constructor(private service:MusicService) {
  
  }

  search(query){
    this.service.search(query)
  }

  query$

  ngOnInit() {
    this.query$ = this.service.queries$

  }
}