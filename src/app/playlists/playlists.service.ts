import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Playlist } from './playlist';

@Injectable()
export class PlaylistsService {

  constructor() { }

  getPlaylist(id){
    return Observable.of(this.playlists.find(
      playlist => playlist.id == id
    ))
  }

  getPlaylists(){
    return Observable.of(this.playlists)
  }

  update(playlist){
    let index = this.playlists.findIndex(p => p.id == playlist.id)
    this.playlists.splice(index,1,playlist)
  }

  playlists:Playlist[] = [
    {
      id: 1, name:'Angular hits', color:"#FF0000", favourite: false
    },
    {
      id: 2, name:'The best of Angular', color:"#00FF00", favourite: true
    },
    {
      id: 3, name:'Angular top 20', color:"#0000FF", favourite: false
    }
  ]


}
