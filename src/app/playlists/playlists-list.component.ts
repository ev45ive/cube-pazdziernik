import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Playlist } from './playlist';

@Component({
  selector: 'playlists-list',
  template: `
    <ul class="list-group">
      <li *ngFor="let playlist of playlists; let i = index" 
      
      (click)="select(playlist)"
      [class.active]=" selected?.id == playlist.id "

      class="list-group-item">
         {{i + 1}}. {{playlist.name}}
      </li>
    </ul>
  `,
  // inputs:['playlists:playlists']
})
export class PlaylistsListComponent implements OnInit {

  @Input()
  selected:Playlist;

  select(playlist:Playlist){
    this.selectedChange.emit(playlist)
  }

  @Output('selectedChange')
  selectedChange = new EventEmitter<Playlist>()

  @Input('playlists')
  playlists:Playlist[] = [ ]

  constructor() { }

  ngOnInit() {
  }

}
