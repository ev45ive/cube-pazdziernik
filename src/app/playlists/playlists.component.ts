import { PlaylistsService } from './playlists.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Playlist } from './playlist';

@Component({
  selector: 'playlists',
  template: `
  <div class="row">
    <div class="col">
      <playlists-list
      (selectedChange)="select($event)"
      [selected]="selected$ | async "
      [playlists]="playlists$ | async"></playlists-list> 
    </div>
    <div class="col">
        <router-outlet></router-outlet>    
    </div>
  </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  playlists$

  constructor(private router:Router,
    private route:ActivatedRoute,
    private service:PlaylistsService) {
    this.playlists$ = this.service.getPlaylists()
  }

  select(playlist){
    this.router.navigate(['playlists', playlist.id])
  }

  selected$;


  ngOnInit() {
    this.selected$ = this.route.firstChild.params
    .pluck('id')
    .map(id => parseInt(<string>id))
    .switchMap(id => this.service.getPlaylist(id) )
  }

}
