import { Component, EventEmitter, group, Input, OnInit, Output } from '@angular/core';
import { Playlist } from './playlist';

@Component({
  selector: 'playlist-details',
  template: `

  <div *ngIf="mode == 'show' ">
    <div class="form-group">
      <label>Name</label>
      <div class="form-control-static">
        {{ playlist.name }}
      </div>
    </div>
    <div class="form-group">
      <label>Favourite</label>
      <div class="form-control-static">
        {{ playlist.favourite? 'YES' : 'NO' }}
      </div>
    </div>
    <div class="form-group">
      <label>Color</label>
      <div class="form-control-static" 
        [style.backgroundColor]=" playlist.color ">
        {{ playlist.color }}
      </div>
    </div>
    <button class="btn btn-success" (click)="edit($event)">Edit</button>
  </div>
  
  <div *ngIf="mode == 'edit'">
    <div class="form-group">
      <label>Name</label>
      <input class="form-control" [(ngModel)]="playlist.name">
    </div>
    <div class="form-group">
      <input type="checkbox" [(ngModel)]="playlist.favourite">
      <label>Favourite</label>
    </div>
    <div class="form-group">
      <label>Color</label>
      <input type="color" [(ngModel)]="playlist.color">
    </div>
    <button class="btn btn-success" (click)="save($event)">Save</button>
    <button class="btn btn-danger" (click)="cancel($event)">Cancel</button>
  </div>
  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {

  @Input('playlist')
  set onPlaylist(playlist){
    this.playlist = this.original_playlist = playlist;
  }

  original_playlist:Playlist

  playlist:Playlist
   
  @Output()
  playlistChange = new EventEmitter<Playlist>()

  mode = 'show'
  save(e){
    console.log('saved!', e)
    this.playlistChange.emit(this.playlist)
  }
  edit(e){
    this.playlist = Object.assign({},this.original_playlist)
    this.mode = 'edit'
  }
  cancel(e){
    this.playlist = this.original_playlist
    this.mode = 'show'
  }

  constructor() { }

  ngOnInit() {
  }

}
