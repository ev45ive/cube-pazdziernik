import { PlaylistsService } from './playlists.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'playlist-container',
  template: `

    <playlist-details *ngIf="(playlist$ | async) as playlist"
    [playlist]="playlist" (playlistChange)="update($event)"
    ></playlist-details>

    <p *ngIf="!playlist">
      Please select playlist
    </p>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  playlist

  playlist$

  update(playlist){
    this.service.update(playlist)
  }

  constructor(private route:ActivatedRoute, private service:PlaylistsService) { }

  ngOnInit() {

    this.playlist$ = this.route.params
    .pluck('id')
    .map(id => parseInt(<string>id))
    .switchMap(id => this.service.getPlaylist(id) )

  }

}
