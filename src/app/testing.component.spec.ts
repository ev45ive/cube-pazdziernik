import { By } from '@angular/platform-browser';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { TestingComponent } from './testing.component';

fdescribe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingComponent ],
      imports:[],
      providers:[
        {provide: 'placki', useValue:'malinowe'}
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use placki service', () => {
    expect(component.service).toEqual('malinowe');
  });

  it('should test service', inject(['placki'],function(placki){
    
    expect(placki).toEqual('malinowe')
  }))

  it('should create', () => {
    var text = fixture.debugElement.query(By.css('p')).nativeElement.textContent
    expect(text).toEqual('placki');
  });

  it('should create', () => {
    component.message = "ojejku"

    fixture.detectChanges()

    var text = fixture.debugElement.query(By.css('p')).nativeElement.textContent
    expect(text).toEqual('ojejku');
  });
});
